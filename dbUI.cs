﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task_19.Model;

namespace Task_19
{
    public partial class dbUI : Form
    {
        /// slNameDBContext = Task_19DBContext /// slNameContext = DBC
        private Task_19DBContext DBC;
        private int ProfessorId;

        public dbUI()
        {
            InitializeComponent();

            DBC = new Task_19DBContext();
            List<Professor> professors = DBC.Professors.ToList();
            List<Student> students = DBC.Students.ToList();

            // The dataGridView for Students
            BindingSource myBindingSourceStud = new BindingSource();
            studentDataGridView.DataSource = myBindingSourceStud;
            myBindingSourceStud.DataSource = DBC.Students.ToList();
            studentDataGridView.Refresh();

            // Adding the professor names onto the combobox.
            foreach (Professor professor in DBC.Professors)
            {
                comboBoxSelectProfessor.Items.Add(professor.Name);
            }
        }

        private void RefreshTable()
        {

            // The dataGridView for Students
            BindingSource myBindingSourceStud = new BindingSource();
            studentDataGridView.DataSource = myBindingSourceStud;
            myBindingSourceStud.DataSource = DBC.Students.ToList();
            studentDataGridView.Refresh();

        }

        private void DataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ComboBoxSelectProfessor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void StudentAddButton_Click(object sender, EventArgs e)
        {
            //new object for EF to use in a DBSet
            Student obj = new Student();
            {
                obj.Name = textStudent.Text;
                obj.ProfessorId =   comboBoxSelectProfessor.SelectedIndex+1;
            };
            DBC.Students.Add(obj);
            DBC.SaveChanges();
            RefreshTable();

        }

        private void StudentDeleteButton_Click(object sender, EventArgs e)
        {
            // find the object in the DBSet you want to delete
            int selectedStud = int.Parse(studentDataGridView.SelectedRows[0].Cells[0].Value.ToString());
            Student tempObj = DBC.Students.Find(selectedStud);
            //Remove it
            DBC.Students.Remove(tempObj);
            DBC.SaveChanges();
            RefreshTable();
            


        }


        private void ButtonSerializeProfessors_Click(object sender, EventArgs e)
        {
            string JSONString = JsonConvert.SerializeObject(DBC.Professors);
            textboxJSON.Text = JSONString;
        }

        private void ProfessorAddButton_Click(object sender, EventArgs e)
        {
            //new object for EF to use in a DBSet
            Professor obj = new Professor();
            {
                obj.Name = textProfessorName.Text;
                obj.Id = obj.Id;
            };
            DBC.Professors.Add(obj);
            DBC.SaveChanges();
            RefreshTable();
        }
    }
}
