namespace Task_19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataProfessors : DbMigration
    {
        public override void Up()
        {

            Sql("INSERT INTO Professors(Name) VALUES('Einstein')");
            Sql("INSERT INTO Professors(Name) VALUES('Hawking')");
            Sql("INSERT INTO Professors(Name) VALUES('Tesla')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Professors WHERE Name ='Einstein'");
            Sql("DELETE FROM Professors WHERE Name ='Hawking'");
            Sql("DELETE FROM Professors WHERE Name ='Tesla'");

        }
    }
}
