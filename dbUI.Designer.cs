﻿namespace Task_19
{
    partial class dbUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentDataGridView = new System.Windows.Forms.DataGridView();
            this.StudentAddButton = new System.Windows.Forms.Button();
            this.StudentDeleteButton = new System.Windows.Forms.Button();
            this.labelStudent = new System.Windows.Forms.Label();
            this.labelProfessorName = new System.Windows.Forms.Label();
            this.labelStudentName = new System.Windows.Forms.Label();
            this.ProfessorAddButton = new System.Windows.Forms.Button();
            this.comboBoxSelectProfessor = new System.Windows.Forms.ComboBox();
            this.textProfessorName = new System.Windows.Forms.TextBox();
            this.textStudent = new System.Windows.Forms.TextBox();
            this.labelSelectProfessor = new System.Windows.Forms.Label();
            this.textboxJSON = new System.Windows.Forms.TextBox();
            this.buttonSerializeProfessors = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.studentDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // studentDataGridView
            // 
            this.studentDataGridView.AllowUserToAddRows = false;
            this.studentDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentDataGridView.Location = new System.Drawing.Point(422, 33);
            this.studentDataGridView.MultiSelect = false;
            this.studentDataGridView.Name = "studentDataGridView";
            this.studentDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentDataGridView.Size = new System.Drawing.Size(342, 322);
            this.studentDataGridView.TabIndex = 1;
            this.studentDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2_CellContentClick);
            // 
            // StudentAddButton
            // 
            this.StudentAddButton.Location = new System.Drawing.Point(293, 275);
            this.StudentAddButton.Name = "StudentAddButton";
            this.StudentAddButton.Size = new System.Drawing.Size(95, 25);
            this.StudentAddButton.TabIndex = 2;
            this.StudentAddButton.Text = "Add";
            this.StudentAddButton.UseVisualStyleBackColor = true;
            this.StudentAddButton.Click += new System.EventHandler(this.StudentAddButton_Click);
            // 
            // StudentDeleteButton
            // 
            this.StudentDeleteButton.Location = new System.Drawing.Point(293, 330);
            this.StudentDeleteButton.Name = "StudentDeleteButton";
            this.StudentDeleteButton.Size = new System.Drawing.Size(95, 25);
            this.StudentDeleteButton.TabIndex = 3;
            this.StudentDeleteButton.Text = "Delete";
            this.StudentDeleteButton.UseVisualStyleBackColor = true;
            this.StudentDeleteButton.Click += new System.EventHandler(this.StudentDeleteButton_Click);
            // 
            // labelStudent
            // 
            this.labelStudent.AutoSize = true;
            this.labelStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labelStudent.Location = new System.Drawing.Point(537, 1);
            this.labelStudent.Name = "labelStudent";
            this.labelStudent.Size = new System.Drawing.Size(113, 29);
            this.labelStudent.TabIndex = 8;
            this.labelStudent.Text = "Students:";
            // 
            // labelProfessorName
            // 
            this.labelProfessorName.AutoSize = true;
            this.labelProfessorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelProfessorName.Location = new System.Drawing.Point(130, 44);
            this.labelProfessorName.Name = "labelProfessorName";
            this.labelProfessorName.Size = new System.Drawing.Size(125, 20);
            this.labelProfessorName.TabIndex = 9;
            this.labelProfessorName.Text = "Professor name:";
            // 
            // labelStudentName
            // 
            this.labelStudentName.AutoSize = true;
            this.labelStudentName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelStudentName.Location = new System.Drawing.Point(130, 202);
            this.labelStudentName.Name = "labelStudentName";
            this.labelStudentName.Size = new System.Drawing.Size(116, 20);
            this.labelStudentName.TabIndex = 10;
            this.labelStudentName.Text = "Student Name:";
            // 
            // ProfessorAddButton
            // 
            this.ProfessorAddButton.Location = new System.Drawing.Point(293, 70);
            this.ProfessorAddButton.Name = "ProfessorAddButton";
            this.ProfessorAddButton.Size = new System.Drawing.Size(95, 25);
            this.ProfessorAddButton.TabIndex = 11;
            this.ProfessorAddButton.Text = "Add";
            this.ProfessorAddButton.UseVisualStyleBackColor = true;
            this.ProfessorAddButton.Click += new System.EventHandler(this.ProfessorAddButton_Click);
            // 
            // comboBoxSelectProfessor
            // 
            this.comboBoxSelectProfessor.FormattingEnabled = true;
            this.comboBoxSelectProfessor.Location = new System.Drawing.Point(267, 233);
            this.comboBoxSelectProfessor.Name = "comboBoxSelectProfessor";
            this.comboBoxSelectProfessor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectProfessor.TabIndex = 12;
            this.comboBoxSelectProfessor.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSelectProfessor_SelectedIndexChanged);
            // 
            // textProfessorName
            // 
            this.textProfessorName.Location = new System.Drawing.Point(261, 44);
            this.textProfessorName.Name = "textProfessorName";
            this.textProfessorName.Size = new System.Drawing.Size(127, 20);
            this.textProfessorName.TabIndex = 13;
            // 
            // textStudent
            // 
            this.textStudent.Location = new System.Drawing.Point(261, 204);
            this.textStudent.Name = "textStudent";
            this.textStudent.Size = new System.Drawing.Size(127, 20);
            this.textStudent.TabIndex = 14;
            // 
            // labelSelectProfessor
            // 
            this.labelSelectProfessor.AutoSize = true;
            this.labelSelectProfessor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelSelectProfessor.Location = new System.Drawing.Point(130, 234);
            this.labelSelectProfessor.Name = "labelSelectProfessor";
            this.labelSelectProfessor.Size = new System.Drawing.Size(125, 20);
            this.labelSelectProfessor.TabIndex = 15;
            this.labelSelectProfessor.Text = "Select professor";
            // 
            // textboxJSON
            // 
            this.textboxJSON.Location = new System.Drawing.Point(15, 333);
            this.textboxJSON.Multiline = true;
            this.textboxJSON.Name = "textboxJSON";
            this.textboxJSON.Size = new System.Drawing.Size(198, 105);
            this.textboxJSON.TabIndex = 17;
            // 
            // buttonSerializeProfessors
            // 
            this.buttonSerializeProfessors.Location = new System.Drawing.Point(15, 304);
            this.buttonSerializeProfessors.Name = "buttonSerializeProfessors";
            this.buttonSerializeProfessors.Size = new System.Drawing.Size(75, 23);
            this.buttonSerializeProfessors.TabIndex = 18;
            this.buttonSerializeProfessors.Text = "Serialize";
            this.buttonSerializeProfessors.UseVisualStyleBackColor = true;
            this.buttonSerializeProfessors.Click += new System.EventHandler(this.ButtonSerializeProfessors_Click);
            // 
            // dbUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSerializeProfessors);
            this.Controls.Add(this.textboxJSON);
            this.Controls.Add(this.labelSelectProfessor);
            this.Controls.Add(this.textStudent);
            this.Controls.Add(this.textProfessorName);
            this.Controls.Add(this.comboBoxSelectProfessor);
            this.Controls.Add(this.ProfessorAddButton);
            this.Controls.Add(this.labelStudentName);
            this.Controls.Add(this.labelProfessorName);
            this.Controls.Add(this.labelStudent);
            this.Controls.Add(this.StudentDeleteButton);
            this.Controls.Add(this.StudentAddButton);
            this.Controls.Add(this.studentDataGridView);
            this.Name = "dbUI";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.studentDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView studentDataGridView;
        private System.Windows.Forms.Button StudentAddButton;
        private System.Windows.Forms.Button StudentDeleteButton;
        private System.Windows.Forms.Label labelStudent;
        private System.Windows.Forms.Label labelProfessorName;
        private System.Windows.Forms.Label labelStudentName;
        private System.Windows.Forms.Button ProfessorAddButton;
        private System.Windows.Forms.ComboBox comboBoxSelectProfessor;
        private System.Windows.Forms.TextBox textProfessorName;
        private System.Windows.Forms.TextBox textStudent;
        private System.Windows.Forms.Label labelSelectProfessor;
        private System.Windows.Forms.TextBox textboxJSON;
        private System.Windows.Forms.Button buttonSerializeProfessors;
    }
}

