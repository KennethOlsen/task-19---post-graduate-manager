﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19.Model
{
    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Professor professor { get; set; }
        public int ProfessorId { get; set; }
         
    }
}
