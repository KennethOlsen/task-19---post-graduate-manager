namespace Task_19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStudents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProfessorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Professors", t => t.ProfessorId, cascadeDelete: true)
                .Index(t => t.ProfessorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "ProfessorId", "dbo.Professors");
            DropIndex("dbo.Students", new[] { "ProfessorId" });
            DropTable("dbo.Students");
        }
    }
}
